#ifndef CONSTANTES_H
#define CONSTANTES_H

#include "raylib.h"

namespace sokoban
{
	namespace constantes
	{
		namespace MENU
		{
			enum MENU_ { PRINCIPAL, JUEGO, EDITOR, CARGA, LIMITE };
		}
		namespace BTN_MENUPRINCIPAL
		{
			enum BOTONES_MENUPRINCIPAL_ { JUGAR, EDITOR, CARGA, SALIR, LIMITE };
		}

		namespace DIRECCION
		{
			enum DIRECCION_ { ARRIBA, DERECHA, ABAJO, IZQUIERDA, LIMITE };
		}
		namespace FONDOS
		{
			enum FONDOS_ { FONDO_MENU, FONDO_JUEGO, LIMITE };
		}
		namespace TILES
		{
			enum TILES_ { SUELO_MIN, SUELO_MAX, OBJETIVO, PASTO, ESQP, ESQC, LAT, AGUJERO, LIMITE };
		}
		namespace OBJTIPO
		{
			enum OBJTIPO_ { JUGADOR, AUTO, LIMITE };
		}
		
		// ----- Mapa -----

		const int maxCol = 24;
		const int maxFil = 16;

		// ----- Animaciones -----

		const int animRotJugador = 5;
		const int velAnimRotJugador = 10;

		const int animMovJugador = 20;
		const int velAnimMovJugador = 5;

		// ----- Niveles -----

		const int maxNiveles = 10;
		const int maxObjetos = 30;

		// ----- Panel Superior -----

		const int alturaPS = 3;
		const int espaciadoPS = 7;
		const int anchoPS = 10;
		
		// ----- Texturas -----

		const int autosPix = 680;
		const int maxAutosH = 4;
		const int maxAutosV = 35;

		const int storageInicioNivel = 10;
		const int storageTamaņoNiveles = 836;

		// ----- Menu -----

		const int botonesMax = 10;

		// ----- Textos -----

		const int tamFuenteMenu = 50;
		const int  tamFuente = 25;
	}

	using namespace constantes;
}

#endif
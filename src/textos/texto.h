#ifndef TEXTO_H
#define TEXTO_H
#include <iostream>
using namespace std;

namespace match
{
	namespace txt
	{
		enum class LENGUAJE { INGLES, ESPA�OL, LIMITE };
		extern LENGUAJE lenguaje;

		extern int tamFuente;

		extern string titulo;
		extern string btnJugar;
		extern string btnOpciones;
		extern string btnCreditos;
		extern string btnSalir;
		extern string empezarPartida;

		extern string muteMusica;
		extern string muteEfectos;

		extern string chresoluciones;

		void actualizarLenguaje();
	}
}

#endif
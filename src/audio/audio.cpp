#include "audio.h"
#include "menu/menues.h"

namespace sokoban
{
	namespace audio
	{
		MUSICA musica[static_cast<int>(TIPOMUSICA::LIMITE)];
		EFECTOS efecto[static_cast<int>(TIPOEFECTO::LIMITE)];

		bool musicas = true;
		bool efectos = true;

		void init()
		{
			musica[static_cast<int>(TIPOMUSICA::MENU)].musica = LoadMusicStream("res/assets/sound/musicmainmenu.mp3");
			musica[static_cast<int>(TIPOMUSICA::JUEGO)].musica = LoadMusicStream("res/assets/sound/musichistory.mp3");
			musica[static_cast<int>(TIPOMUSICA::EDITOR)].musica = LoadMusicStream("res/assets/sound/musiceditor.mp3");

			PlayMusicStream(musica[0].musica);
			PlayMusicStream(musica[1].musica);
			PlayMusicStream(musica[2].musica);

			SetMusicVolume(musica[0].musica, 0.5f);
			SetMusicVolume(musica[1].musica, 0.5f);
			SetMusicVolume(musica[2].musica, 0.5f);

			efecto[static_cast<int>(TIPOEFECTO::ROTAR)].efecto = LoadSound("res/assets/sound/fxrotation.wav");
			efecto[static_cast<int>(TIPOEFECTO::MOVER)].efecto = LoadSound("res/assets/sound/fxmove.wav");
			efecto[static_cast<int>(TIPOEFECTO::EMPUJAR)].efecto = LoadSound("res/assets/sound/fxclacson.wav");
		}

		void update()
		{
			if (musicas)
			{
				if (menues::menuActual == MENU::JUEGO || menues::menuActual == MENU::CARGA)
				{
					UpdateMusicStream(musica[static_cast<int>(TIPOMUSICA::JUEGO)].musica);
				}
				else if (menues::menuActual == MENU::EDITOR)
				{
					UpdateMusicStream(musica[static_cast<int>(TIPOMUSICA::EDITOR)].musica);
				}
				else if (menues::menuActual == MENU::PRINCIPAL)
				{
					UpdateMusicStream(musica[static_cast<int>(TIPOMUSICA::MENU)].musica);
				}
			}

			if (efectos)
			{
				for (int i = 0; i < static_cast<int>(TIPOEFECTO::LIMITE); i++)
				{
					if (efecto[i].activo)
					{
						efecto[i].activo = false;
						PlaySound(efecto[i].efecto);
					}
				}
			}
		}

		void deinit()
		{
			for (int i = 0; i < static_cast<int>(TIPOMUSICA::LIMITE); i++)
			{
				UnloadMusicStream(musica[i].musica);
			}

			for (int i = 0; i < static_cast<int>(TIPOEFECTO::LIMITE); i++)
			{
				UnloadSound(efecto[i].efecto);
			}
		}
	}
}
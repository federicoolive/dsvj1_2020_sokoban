#ifndef ESTRUCTURAS_H
#define ESTRUCTURAS_H

#include "raylib.h"
namespace sokoban
{
	namespace stt
	{
		struct BOTON
		{
			Rectangle rec;
			Color color = WHITE;
		};

		struct Vecint2
		{
			int x = 0;
			int y = 0;
		};
	}

	using namespace stt;
}

#endif
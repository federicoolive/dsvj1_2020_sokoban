#include "animaciones.h"
#include <time.h>
#include "juego/objetos.h"
#include "origen/loop.h"
#include "configuraciones/constantes.h"
#include "juego/nivelesManager.h"

namespace sokoban
{
	namespace anim
	{
		int rotClock;
		int movClock;

		void movimientoManager()
		{
			if (obj::jugador.enRotacion)
			{
				if (loop::clockUpdate > rotClock + animRotJugador)
				{
					rotClock = clock();
					obj::jugador.enRotacion = obj::rotarJugador();
				}
			}
			else if (obj::jugador.enMovimiento)
			{
				if (loop::clockUpdate > movClock + animMovJugador)
				{
					movClock = clock();
					obj::jugador.enMovimiento = obj::moverJugador();
				}
			}
		}


	}
}
#include "juego.h"
#include <iostream>
#include "texturas/texturas.h"
#include "juego/objetos.h"
#include "tiles/tiles.h"
#include "animaciones/animaciones.h"
#include "nivelesManager.h"
#include "menu/menues.h"
#include "audio/audio.h"

using namespace std;

namespace sokoban
{
	namespace juego
	{
		namespace historia
		{
			bool selectorNiveles = false;
			void init()
			{
				tiles::init();
				nivelm::init();
				obj::init();
				nivelm::nivel.tiempos[0] = clock();
				nivelm::nivel.movimientos = 0;
				nivelm::nivel.completado = false;
				selectorNiveles = false;
				menues::selectorNiveles::init();
				if (!menues::selectorCarga::panelActivo)
				{
					if (nivelm::nivelActual < 2)
					{
						tiles::setColor(WHITE);
					}
					else if (nivelm::nivelActual < 4)
					{
						tiles::setColor(BLUE);
					}
					else if (nivelm::nivelActual < 6)
					{
						tiles::setColor(RAYWHITE);
					}
					else
					{
						tiles::setColor(WHITE);
					}
				}
			}

			void play()
			{
				input();
				update();
				draw();
			}

			void update()
			{
				if (!obj::jugador.enMovimiento)
				{
					if (chequeoVictoria())
					{
						if (!nivelm::nivel.completado)
						{
							nivelm::guardarNivelActual();
						}
						nivelm::nivel.completado = true;
						selectorNiveles = true;
						menues::selectorNiveles::init();
					}
				}
				
				if (!nivelm::nivel.completado && !selectorNiveles)
				{
					nivelm::nivel.tiempos[1] = clock();
				}

				if (selectorNiveles)
				{
					menues::selectorNiveles::update();
				}
				if (!selectorNiveles)
				{
					obj::update();
					anim::movimientoManager();
				}

				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					if (CheckCollisionPointRec(tiles::tile[static_cast<int>(obj::autos[i].pos.x / tiles::lado)][static_cast<int>(obj::autos[i].pos.y / tiles::lado)].pos, obj::autos[i].destRec))
					{
						obj::autos[i].visibilidad = true;
					}
					else
					{
						obj::autos[i].visibilidad = false;
					}
				}
			}

			void input()
			{
				if (!selectorNiveles)
				{
					if (!obj::jugador.enMovimiento && !obj::jugador.enRotacion)
					{
						if (IsKeyDown(KEY_UP))
						{
							if (obj::jugador.direccion != DIRECCION::ARRIBA)
							{
								obj::jugador.enRotacion = true;
								obj::jugador.direccion = DIRECCION::ARRIBA;
								obj::jugador.rotObjetivo = 0;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::ROTAR)].activo = true;
							}

							if (celdaValida(obj::jugador.posObjetivo.x, obj::jugador.posObjetivo.y - tiles::lado, true))
							{
								obj::jugador.enMovimiento = true;
								obj::jugador.posObjetivo.y -= tiles::lado;
								nivelm::nivel.movimientos++;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::MOVER)].activo = true;
							}
						}
						else if (IsKeyDown(KEY_RIGHT))
						{
							if (obj::jugador.direccion != DIRECCION::DERECHA)
							{
								obj::jugador.direccion = DIRECCION::DERECHA;
								obj::jugador.enRotacion = true;
								obj::jugador.rotObjetivo = 90;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::ROTAR)].activo = true;
							}

							if (celdaValida(obj::jugador.posObjetivo.x + tiles::lado, obj::jugador.posObjetivo.y, true))
							{
								obj::jugador.enMovimiento = true;
								obj::jugador.posObjetivo.x += tiles::lado;
								nivelm::nivel.movimientos++;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::MOVER)].activo = true;
							}
						}
						else if (IsKeyDown(KEY_DOWN))
						{
							if (obj::jugador.direccion != DIRECCION::ABAJO)
							{
								obj::jugador.direccion = DIRECCION::ABAJO;
								obj::jugador.enRotacion = true;
								obj::jugador.rotObjetivo = 180;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::ROTAR)].activo = true;
							}

							if (celdaValida(obj::jugador.posObjetivo.x, obj::jugador.posObjetivo.y + tiles::lado, true))
							{
								obj::jugador.enMovimiento = true;
								obj::jugador.posObjetivo.y += tiles::lado;
								nivelm::nivel.movimientos++;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::MOVER)].activo = true;
							}
						}
						else if (IsKeyDown(KEY_LEFT))
						{
							if (obj::jugador.direccion != DIRECCION::IZQUIERDA)
							{
								obj::jugador.direccion = DIRECCION::IZQUIERDA;
								obj::jugador.enRotacion = true;
								obj::jugador.rotObjetivo = 270;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::ROTAR)].activo = true;
							}

							if (celdaValida(obj::jugador.posObjetivo.x - tiles::lado, obj::jugador.posObjetivo.y, true))
							{
								obj::jugador.enMovimiento = true;
								obj::jugador.posObjetivo.x -= tiles::lado;
								nivelm::nivel.movimientos++;
								audio::efecto[static_cast<int>(audio::TIPOEFECTO::MOVER)].activo = true;
							}
						}
						else if (IsKeyPressed(KEY_SPACE))
						{
							if (!nivelm::nivel.completado)
							{
								selectorNiveles = !selectorNiveles;
							}
						}
						
					}
				}
				else if (IsKeyPressed(KEY_SPACE))
				{
					if (!nivelm::nivel.completado)
					{
						selectorNiveles = !selectorNiveles;
					}
				}
			}

			bool celdaValida(int posObjetivox, int posObjetivoy, bool dobleChequeo)
			{
				if (tiles::tile[static_cast<int>(posObjetivox / tiles::lado)][static_cast<int>(posObjetivoy / tiles::lado)].tipo <= TILES::OBJETIVO)
				{
					bool hayAuto = false;
					int autoIndex = 0;
					for (int i = 0; i < nivelm::nivel.maxObj; i++)
					{
						if (obj::autos[i].pos.x == posObjetivox && obj::autos[i].pos.y == posObjetivoy)
						{
							hayAuto = true;
							autoIndex = i;
							break;
						}
					}

					if (hayAuto)
					{
						switch (obj::jugador.direccion)
						{
						case DIRECCION::ARRIBA:

							posObjetivoy -= tiles::lado;

							break;
						case DIRECCION::ABAJO:

							posObjetivoy += tiles::lado;

							break;
						case DIRECCION::DERECHA:

							posObjetivox += tiles::lado;

							break;
						case DIRECCION::IZQUIERDA:

							posObjetivox -= tiles::lado;

							break;
						default:
							break;
						}

						if (dobleChequeo)
						{
							if (celdaValida(posObjetivox, posObjetivoy, false))
							{
								obj::autos[autoIndex].posObjetivo.x = posObjetivox;
								obj::autos[autoIndex].posObjetivo.y = posObjetivoy;
								obj::autos[autoIndex].enMovimiento = true;
								return true;
							}
							else
							{
								return false;
							}
						}
						else
						{
							false;
						}
					}
					else
					{
						return true;
					}
				}

				return false;
			}

			bool chequeoVictoria()
			{
				int contadorAutosPosicionados = 0;
				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					int posX = obj::autos[i].pos.x / tiles::lado;
					int posY = obj::autos[i].pos.y / tiles::lado;

					if (tiles::tile[posX][posY].tipo == TILES::OBJETIVO)
					{
						contadorAutosPosicionados++;
					}
				}

				if (contadorAutosPosicionados == nivelm::nivel.maxObj)
				{
					return true;
				}
				return false;
			}

			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);

				tiles::draw();
				obj::draw();

				if (selectorNiveles)
				{
					menues::selectorNiveles::draw();
				}

				DrawText(TextFormat("Moves: %2i / %2i", nivelm::nivel.movimientos, nivelm::nivel.movMax), tiles::tile[8][0].pos.x, tiles::tile[8][0].pos.y - tamFuente / 2, tamFuente, BLACK);

				cronometro(nivelm::nivel.tiempos[1] - nivelm::nivel.tiempos[0], nivelm::nivel.tiempoMax);

				EndDrawing();
			}

			void cronometro(int tiempo, int tiempoMax)
			{
				int seg1 = tiempo / 1000;
				int min1 = 0;
				while (seg1 >= 60)
				{
					min1++;
					seg1 -= 60;
				}

				int seg2 = tiempoMax / 1000;
				int min2 = 0;
				while (seg2 >= 60)
				{
					min2++;
					seg2 -= 60;
				}
				
				DrawText(TextFormat("Time: %2i:%02i / %2i:%02i", min1, seg1, min2, seg2), tiles::tile[8][0].pos.x, tiles::tile[8][1].pos.y - tamFuente / 2, tamFuente, BLACK);
			}

			void deinit()
			{

			}

		}

		namespace editor
		{
			tiles::MAPA edit;

			void init()
			{
				nivelm::nivel.maxObj = 0;
				tx::init();
				tiles::init();
				nivelm::setPasto();
				nivelm::setPanelSuperior();
				obj::init();
				nivelm::setPanelEditor();
				edit.color = WHITE;
				edit.pos = { 0, 0 };
				edit.rotacion = 0;
				edit.tipo = TILES::PASTO;
			}

			void play()
			{
				input();
				update();
				draw();
			}

			void update()
			{
				
			}

			void input()
			{
				Vecint2 pos = { mouse.x / tiles::lado,mouse.y / tiles::lado };
				Vector2 posf = { pos.x * tiles::lado + tiles::lado / 2 , pos.y * tiles::lado + tiles::lado / 2 };
				// Input Tile - Click Izq Drop - Click Der Pick

				// Se absorve las propiedades del tile
				if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON))
				{
					edit = tiles::tile[pos.x][pos.y];
				}// Se ponen los Tiles:
				else if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				{
					if (pos.y >= alturaPS && pos.y < maxFil - 1 && pos.x > 0 && pos.x < maxCol - 1)
					{
						if (edit.tipo >= TILES::SUELO_MIN && edit.tipo <= TILES::PASTO || edit.tipo == TILES::AGUJERO)
						{
							edit.rotacion = 90 * GetRandomValue(0, 3);
						}
						edit.pos.x = posf.x;
						edit.pos.y = posf.y;
						tiles::tile[pos.x][pos.y] = edit;
					}
				}
				else if (IsKeyPressed(KEY_R) || IsKeyPressed(KEY_R+32))
				{
					tiles::tile[pos.x][pos.y].rotacion += 90;
					if (tiles::tile[pos.x][pos.y].rotacion >= 360)
					{
						tiles::tile[pos.x][pos.y].rotacion -= 360;
					}
				}

				if (IsKeyPressed(KEY_ZERO))
				{
					bool hayAuto = false;
					for (int i = 0; i < nivelm::nivel.maxObj; i++)
					{
						if (!hayAuto)
						{
							Rectangle autoCollider = { obj::autos[i].destRec.x - tiles::lado / 2, obj::autos[i].destRec.y - tiles::lado / 2, obj::autos[i].destRec.width , obj::autos[i].destRec.height };
							if (CheckCollisionPointRec(mouse, autoCollider))
							{
								hayAuto = true;
								obj::autos[i] = obj::autos[i + 1];
							}
						}
						else
						{
							obj::autos[i] = obj::autos[i + 1];
						}
					}

					if (hayAuto)
					{
						nivelm::nivel.maxObj--;
					}
				}

				if (IsKeyPressed(KEY_ONE))
				{
					bool hayObjeto = false;
					for (int i = 0; i < nivelm::nivel.maxObj; i++)
					{
						if (obj::autos[i].pos.x == posf.x && obj::autos[i].pos.y == posf.y)
						{
							hayObjeto = true;
						}
					}

					if (!hayObjeto)
					{
						obj::jugador.pos = { posf.x, posf.y };
						obj::jugador.destRec.x = posf.x;
						obj::jugador.destRec.y = posf.y;
					}
				}
				if (IsKeyPressed(KEY_TWO))
				{
					if (nivelm::nivel.maxObj < maxObjetos - 1)
					{
						bool hayObjeto = false;
						for (int i = 0; i < nivelm::nivel.maxObj; i++)
						{
							if (obj::autos[i].pos.x == posf.x && obj::autos[i].pos.y == posf.y)
							{
								hayObjeto = true;
							}
						}

						if (obj::jugador.pos.x == posf.x && obj::jugador.pos.y == posf.y)
						{
							hayObjeto = true;
						}

						if (!hayObjeto)
						{
							obj::establecerUnObjeto(nivelm::nivel.maxObj);
							obj::autos[nivelm::nivel.maxObj].pos = { posf.x, posf.y };
							obj::autos[nivelm::nivel.maxObj].destRec.x = posf.x;
							obj::autos[nivelm::nivel.maxObj].destRec.y = posf.y;

							nivelm::nivel.maxObj++;
						}
					}
				}
				generadorDeHistoria();
			}

			void generadorDeHistoria()
			{
				if (IsKeyPressed(KEY_KP_1))
				{
					nivelm::nivelActual = 11;
					nivelm::guardarData();
					menues::menuActual = MENU::PRINCIPAL;
				}
				if (IsKeyPressed(KEY_KP_2))
				{
					nivelm::nivelActual = 12;
					nivelm::guardarData();
					menues::menuActual = MENU::PRINCIPAL;
				}
				if (IsKeyPressed(KEY_KP_3))
				{
					nivelm::nivelActual = 13;
					nivelm::guardarData();
					menues::menuActual = MENU::PRINCIPAL;
				}
				if (IsKeyPressed(KEY_KP_4))
				{
					nivelm::nivelActual = 14;
					nivelm::guardarData();
					menues::menuActual = MENU::PRINCIPAL;
				}
				if (IsKeyPressed(KEY_KP_5))
				{
					nivelm::nivelActual = 15;
					nivelm::guardarData();
					menues::menuActual = MENU::PRINCIPAL;
				}
				if (IsKeyPressed(KEY_SPACE))
				{
					menues::principal::init();
					menues::principal::panelActivo = false;
					menues::selectorCarga::panelActivo = false;
					menues::menuActual = MENU::PRINCIPAL;
				}
			}

			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);

				tiles::draw();
				obj::draw();

				DrawText("R: Rotate", tiles::tile[1][0].pos.x, tiles::tile[0][0].pos.y - tamFuente / 2, tamFuente * 1.5, BLACK);
				DrawText("Left Click: Drop Tile", tiles::tile[8][0].pos.x, tiles::tile[0][0].pos.y - tamFuente / 2, tamFuente , BLACK);
				DrawText("Right Click Pick Tile", tiles::tile[8][0].pos.x, tiles::tile[0][1].pos.y - tamFuente / 2, tamFuente , BLACK);
				DrawText("1: Set Player Position", tiles::tile[17][0].pos.x, tiles::tile[0][0].pos.y - tamFuente / 2, tamFuente , BLACK);
				DrawText("2: Set Cars Position", tiles::tile[17][0].pos.x, tiles::tile[0][1].pos.y - tamFuente / 2, tamFuente , BLACK);

				DrawText("Save: KeyBorad Numbers 1-5 Positions. It may delay. Space to Cancel.", tiles::tile[1][0].pos.x, tiles::tile[0][maxFil-1].pos.y - tamFuente / 2, tamFuente, BLACK);

				EndDrawing();
			}

			void deinit()
			{

			}
		}		
	}
}
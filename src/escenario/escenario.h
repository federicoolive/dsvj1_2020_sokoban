#ifndef ESCENARIO_H
#define ESCENARIO_H

#include "raylib.h"
#include "configuraciones/constantes.h"

namespace sokoban
{
	namespace escenario
	{
		extern Rectangle panelMatch;
		extern Rectangle panelPausa;

		

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif
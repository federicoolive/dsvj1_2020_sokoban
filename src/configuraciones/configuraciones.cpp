#include "configuraciones.h"
#include "escenario/escenario.h"
#include "menu/menues.h"
#include "juego/juego.h"
#include "audio/audio.h"
#include "textos/texto.h"
#include "juego/objetos.h"
#include "juego/nivelesManager.h"
#include "texturas/texturas.h"

namespace sokoban
{
	namespace config
	{
		Vector2 posCentro;

		Vector2 resoluciones[5];
		int resolActual;
		int fpsTarget = 60;
		float escalado;

		void init()
		{
			resoluciones[0] = { 960,  640 };
			resoluciones[1] = { 394,  700 };
			resoluciones[2] = { 450,  800 };
			resoluciones[3] = { 506,  900 };
			resoluciones[4] = { 563, 1000 };
			resolActual = 0;

			InitWindow(resoluciones[resolActual].x, resoluciones[resolActual].y, "Sokoban");
			posCentro.x = GetWindowPosition().x + resoluciones[resolActual].x / 2;
			posCentro.y = GetWindowPosition().y + resoluciones[resolActual].y / 2;

			SetTargetFPS(fpsTarget);
			update();
		}

		void update()
		{
			SetWindowSize(resoluciones[resolActual].x, resoluciones[resolActual].y);
			SetWindowPosition(posCentro.x - resoluciones[resolActual].x / 2, posCentro.y - resoluciones[resolActual].y / 2);

			escalado = resoluciones[resolActual].x / resoluciones[0].x;

			//------ Inits ------
			menues::init();
		}

		void deInit()
		{
			tx::deinit();
			audio::deinit();
		}

		void preCargaData()
		{
			int indexData = 0;
			nivelm::nivelesDesbloqueados = LoadStorageValue(indexData);

			for (int i = 0; i < nivelm::nivelesDesbloqueados; i++)
			{
				indexData = storageInicioNivel + i * storageTamaņoNiveles;

				int tiempo[2] = { LoadStorageValue(indexData), LoadStorageValue(indexData + 2) };

				int movimientos[2] = { LoadStorageValue(indexData + 1) ,LoadStorageValue(indexData + 3) };

				nivelm::lvlStats[i].completado = LoadStorageValue(indexData + 4);

				if (nivelm::lvlStats[i].completado)
				{
					nivelm::lvlStats[i].movimientos = (tiempo[0] < tiempo[1]) ? true : false;
					nivelm::lvlStats[i].tiempo = (movimientos[0] < movimientos[1]) ? true : false;
				}
				else
				{
					nivelm::lvlStats[i].movimientos = false;
					nivelm::lvlStats[i].tiempo = false;
				}
			}
		}
	}
}
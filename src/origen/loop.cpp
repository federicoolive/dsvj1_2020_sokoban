#include "loop.h"
#include <time.h>
#include "configuraciones/general.h"
#include "menu/menues.h"
#include "configuraciones/configuraciones.h"
#include "juego/juego.h"
#include "audio/audio.h"
#include "texturas/texturas.h"
#include "juego/nivelesManager.h"

namespace sokoban
{
	namespace loop
	{
		int clockUpdate;
		bool exit = false;
		void juego()
		{
			//nivelm::reStartNivelesCreados();
			config::init();

			InitAudioDevice();
			audio::init();

			menues::menuActual = MENU::PRINCIPAL;

			tx::init();
			menues::selectorNiveles::init();
			nivelm::cargarJugador();

			while (!WindowShouldClose() && !exit)
			{
				update();
				switch (menues::menuActual)
				{
				case MENU::PRINCIPAL:

					menues::principal::update();
					menues::principal::draw();

					break;
				case MENU::JUEGO:

					juego::historia::play();

					break;

				case MENU::EDITOR:

					juego::editor::play();

					break;
				default:
					break;
				}
			}
			config::deInit();
			CloseWindow();
		}

		void update()
		{
			audio::update();
			clockUpdate = clock();
			mouse = GetMousePosition();
			if (IsKeyPressed(KEY_M))
			{
				audio::musicas = !audio::musicas;
				audio::efectos = !audio::musicas;
			}
		}
	}
}
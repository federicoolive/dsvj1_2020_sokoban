#include "texturas.h"
#include "tiles/tiles.h"

namespace sokoban
{
	namespace tx
	{
		Texture2D btnMenuPrincipal;
		Texture2D btnNiveles;
		Texture2D estrella;
		Texture2D panel;
		Texture2D candado;

		Texture2D fondo[FONDOS::LIMITE];
		Texture2D tile[TILES::LIMITE];
		Texture2D grua;
		Texture2D autos;

		void init()
		{
			btnMenuPrincipal = LoadTexture("res/assets/textures/UI/btn_gral.PNG");
			btnNiveles = LoadTexture("res/assets/textures/UI/btn_niveles.PNG");
			estrella = LoadTexture("res/assets/textures/UI/estrella_lvl.PNG");
			panel = LoadTexture("res/assets/textures/UI/panel.PNG");
			candado = LoadTexture("res/assets/textures/UI/candado.PNG");

			grua = LoadTexture("res/assets/textures/objects/grua.PNG");
			autos = LoadTexture("res/assets/textures/objects/autos.PNG");

			tile[TILES::SUELO_MIN] = LoadTexture("res/assets/textures/tiles/cemento.PNG");
			tile[TILES::SUELO_MAX] = LoadTexture("res/assets/textures/tiles/cemento2.PNG");

			tile[TILES::LAT] = LoadTexture("res/assets/textures/tiles/lateral_pasto_cemento.PNG");

			tile[TILES::ESQP] = LoadTexture("res/assets/textures/tiles/esquina_pasto_cemento.PNG");

			tile[TILES::ESQC] = LoadTexture("res/assets/textures/tiles/esquina_cemento_pasto.PNG");

			tile[TILES::PASTO] = LoadTexture("res/assets/textures/tiles/pasto.PNG");
			tile[TILES::OBJETIVO] = LoadTexture("res/assets/textures/tiles/objetivo.PNG");
			tile[TILES::AGUJERO] = LoadTexture("res/assets/textures/tiles/agujero.PNG");

			update();
		}

		void update()
		{
			grua.width = tiles::lado;
			grua.height = tiles::lado * 2;

			for (int i = 0; i < TILES::LIMITE; i++)
			{
				tile[i].width = tiles::lado;
				tile[i].height = tiles::lado;
			}
		}

		void deinit()
		{
			UnloadTexture(btnMenuPrincipal);
			UnloadTexture(btnNiveles);
			UnloadTexture(estrella);
			UnloadTexture(panel);
			UnloadTexture(candado);
			UnloadTexture(grua);
			UnloadTexture(autos); 
			for (int i = 0; i < TILES::LIMITE; i++)
			{
				UnloadTexture(tile[i]);
			}

			
		}
	}
}
#include "menues.h"
#include "configuraciones/estructuras.h"
#include "configuraciones/general.h"
#include "juego/juego.h"
#include "audio/audio.h"
#include "juego/nivelesManager.h"
#include "texturas/texturas.h"
#include "configuraciones/configuraciones.h"
#include "origen/loop.h"

namespace sokoban
{
	namespace menues
	{
		int menuActual;

		void init()
		{
			tiles::init();
			nivelm::setPasto();
			nivelm::setPanelSuperior();
			principal::init();
			selectorCarga::init();
		}

		namespace principal
		{
			BOTON btn[BTN_MENUPRINCIPAL::LIMITE];
			bool panelActivo = false;

			void init()
			{
				int offset = 0;
				for (int i = 0; i < BTN_MENUPRINCIPAL::LIMITE; i++)
				{
					btn[i].rec.width = tiles::lado * 6;
					btn[i].rec.height = tiles::lado * 2;
					btn[i].color = WHITE;
					btn[i].rec.x = GetScreenWidth() / 2 - btn[i].rec.width / 2;
					if (i == BTN_MENUPRINCIPAL::CARGA)
					{
						offset = tiles::lado;
					}
					else if (i == BTN_MENUPRINCIPAL::SALIR)
					{
						offset = tiles::lado * 2;
					}

					btn[i].rec.y = GetScreenHeight() / 2 + btn[i].rec.height * (i - 2) + offset;
				}
				nivelm::setPasto();
				nivelm::setPanelSuperior();

			}

			void update()
			{
				if (selectorCarga::panelActivo)
				{
					selectorCarga::input();
				}
				else if (panelActivo)
				{
					selectorNiveles::update(); 
				}
				else
				{
					input();
				}
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					for (int i = 0; i < BTN_MENUPRINCIPAL::LIMITE; i++)
					{
						if (CheckCollisionPointRec(mouse, btn[i].rec))
						{
							mouse = mouse;
							switch (i)
							{
							case BTN_MENUPRINCIPAL::JUGAR:

								panelActivo = true;

								break;
							case BTN_MENUPRINCIPAL::EDITOR:

								juego::editor::init();
								menuActual = MENU::EDITOR;

								break;
							case BTN_MENUPRINCIPAL::CARGA:

								selectorCarga::panelActivo = true;

								break;
							case BTN_MENUPRINCIPAL::SALIR:

								loop::exit = true;

								break;
							default:
								break;
							}
						}
					}
				}
				else if (IsKeyPressed(KEY_Z))
				{
					nivelm::reStartNivelesCreados();
				}
			}

			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);
				tiles::draw();
				if (panelActivo)
				{
					selectorNiveles::draw();
				}
				else if (selectorCarga::panelActivo)
				{
					selectorCarga::draw();
				}
				else
				{
					for (int i = 0; i < BTN_MENUPRINCIPAL::LIMITE; i++)
					{
						DrawTexturePro(tx::btnMenuPrincipal, { 0, 0, static_cast<float>(tx::btnMenuPrincipal.width), static_cast<float>(tx::btnMenuPrincipal.height) }, btn[i].rec, { 0, 0 }, 0, btn[i].color);

						if (i == BTN_MENUPRINCIPAL::JUGAR)
							DrawText("Play", btn[i].rec.x + btn[i].rec.width / 2 - MeasureText("Play", tamFuenteMenu) / 2, btn[i].rec.y + btn[i].rec.height / 2 - tamFuenteMenu / 2, tamFuenteMenu, WHITE);
						if (i == BTN_MENUPRINCIPAL::EDITOR)
							DrawText("Editor", btn[i].rec.x + btn[i].rec.width / 2 - MeasureText("Editor", tamFuenteMenu) / 2, btn[i].rec.y + btn[i].rec.height / 2 - tamFuenteMenu / 2, tamFuenteMenu, WHITE);
						if (i == BTN_MENUPRINCIPAL::CARGA)
							DrawText("Load", btn[i].rec.x + btn[i].rec.width / 2 - MeasureText("Load", tamFuenteMenu) / 2, btn[i].rec.y + btn[i].rec.height / 2 - tamFuenteMenu / 2, tamFuenteMenu, WHITE);
						if (i == BTN_MENUPRINCIPAL::SALIR)
							DrawText("Exit", btn[i].rec.x + btn[i].rec.width / 2 - MeasureText("Exit", tamFuenteMenu) / 2, btn[i].rec.y + btn[i].rec.height / 2 - tamFuenteMenu / 2, tamFuenteMenu, WHITE);
					}
				}
				DrawText("Created By", tiles::tile[8][0].pos.x, tiles::tile[0][0].pos.y - tamFuente / 2, tamFuente * 1.5, BLACK);
				DrawText("Federico Olive.", tiles::tile[9][0].pos.x, tiles::tile[0][1].pos.y - tamFuente / 2, tamFuente * 1.5, BLACK);

				DrawText("Menu & Game Music: Patrick Patrikios - Forget Me Not & Cover", tiles::tile[2][0].pos.x, tiles::tile[0][14].pos.y - tamFuente / 2, tamFuente, BLACK);
				DrawText("Edit Music: Nana Kwabena - She No Dull Beat", tiles::tile[5][0].pos.x, tiles::tile[0][15].pos.y - tamFuente / 2, tamFuente, BLACK);

				EndDrawing();
			}

			void deinit()
			{

			}
		}

		namespace selectorNiveles
		{
			BOTON btnMenuPrincipal;
			BOTON btnNiveles[maxNiveles];
			Rectangle panel;

			void init()
			{
				panel = { 40 * 4, tiles::lado * 2, tiles::lado * 16, tiles::lado * 12 };
				btnMenuPrincipal = { tiles::lado * 4.5f,tiles::lado * 2.5f,tiles::lado * 3.0f ,tiles::lado * 1.5f };
				for (int i = 0; i < maxNiveles; i++)
				{
					if (i < 5)
					{
						btnNiveles[i].rec.x = panel.x + tiles::lado + i * tiles::lado * 3;
						btnNiveles[i].rec.y = panel.y + tiles::lado * 4;
					}
					else if (i >= 5)
					{
						btnNiveles[i].rec.x = panel.x + tiles::lado + (i - 5) * tiles::lado * 3;
						btnNiveles[i].rec.y = panel.y + tiles::lado * 8;
					}

					btnNiveles[i].rec.width = tiles::lado * 2;
					btnNiveles[i].rec.height = tiles::lado * 2;
					btnNiveles[i].color = WHITE;
				}
			}

			void update()
			{
				input();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					for (int i = 0; i < maxNiveles; i++)
					{
						if (CheckCollisionPointRec(mouse, btnNiveles[i].rec))
						{
							if (i < nivelm::nivelesDesbloqueados)
							{
								nivelm::nivelActual = i;
								juego::historia::init();
								menuActual = MENU::JUEGO;
							}
						}
					}
					if (CheckCollisionPointRec(mouse, btnMenuPrincipal.rec))
					{
						menues::principal::init();
						menues::principal::panelActivo = false;
						menues::menuActual = MENU::PRINCIPAL;
					}
				}
			}

			void draw()
			{
				DrawTexturePro(tx::panel, { 0, 0, static_cast<float>(tx::panel.width), static_cast<float>(tx::panel.height) }, panel, { 0, 0 }, 0, WHITE);

				for (int i = 0; i < maxNiveles; i++)
				{
					DrawTexturePro(tx::btnNiveles, { static_cast<float>((tx::btnNiveles.width / 10) * i), 0, static_cast<float>(tx::btnNiveles.width / 10), static_cast<float>(tx::btnNiveles.height) }, btnNiveles[i].rec, { 0, 0 }, 0, WHITE);

					Rectangle sourceRec;
					Rectangle destRec = { btnNiveles[i].rec.x, btnNiveles[i].rec.y, tiles::lado , tiles::lado };

					if (i < nivelm::nivelesDesbloqueados)
					{
						destRec.x = btnNiveles[i].rec.x;
						sourceRec = { static_cast<float>(tx::estrella.width / 2 * nivelm::lvlStats[i].movimientos), 0, static_cast<float>(tx::estrella.width / 2), static_cast<float>(tx::estrella.height) };
						DrawTexturePro(tx::estrella, sourceRec, destRec, { destRec.width / 2, destRec.height / 2 }, 0, btnNiveles[i].color);

						sourceRec = { static_cast<float>(tx::estrella.width / 2 * nivelm::lvlStats[i].tiempo), 0, static_cast<float>(tx::estrella.width / 2), static_cast<float>(tx::estrella.height) };
						destRec.x = btnNiveles[i].rec.x + btnNiveles[i].rec.width;
						DrawTexturePro(tx::estrella, sourceRec, destRec, { destRec.width / 2, destRec.height / 2 }, 0, btnNiveles[i].color);

						sourceRec = { static_cast<float>(tx::estrella.width / 2 * nivelm::lvlStats[i].completado), 0, static_cast<float>(tx::estrella.width / 2), static_cast<float>(tx::estrella.height) };
						destRec.x = btnNiveles[i].rec.x + btnNiveles[i].rec.width / 2;
						DrawTexturePro(tx::estrella, sourceRec, destRec, { destRec.width / 2, destRec.height / 2 }, 0, btnNiveles[i].color);
					}
					else
					{
						sourceRec = { 0, 0, static_cast<float>(tx::candado.width), static_cast<float>(tx::candado.height) };
						destRec = { btnNiveles[i].rec.x + btnNiveles[i].rec.width * 1 / 4, btnNiveles[i].rec.y - btnNiveles[i].rec.height * 1 / 4, btnNiveles[i].rec.width / 2, btnNiveles[i].rec.height / 2 };

						DrawTexturePro(tx::candado, sourceRec, destRec, { 0, 0 }, 0, btnNiveles[i].color);
					}
				}

				DrawTexturePro(tx::btnMenuPrincipal, { 0, 0, static_cast<float>(tx::btnMenuPrincipal.width), static_cast<float>(tx::btnMenuPrincipal.height) }, btnMenuPrincipal.rec, { 0, 0 }, 0, btnMenuPrincipal.color);
				DrawText("Go Menu", btnMenuPrincipal.rec.x + btnMenuPrincipal.rec.width / 2 - MeasureText("Go Menu", tamFuente) / 2, btnMenuPrincipal.rec.y + btnMenuPrincipal.rec.height / 2 - tamFuente / 2, tamFuente, btnMenuPrincipal.color);

				DrawText(" Level Selector", panel.x + panel.width / 2 - MeasureText("Level Selector", tamFuente * 1.5f) / 2, tiles::tile[0][2].pos.y + tamFuente * 1.5 / 2, tamFuente * 1.5, WHITE);

			}

			void deinit()
			{

			}
		}

		namespace selectorCarga
		{
			bool panelActivo = false;
			BOTON btnMenuPrincipal;
			BOTON btnNiveles[maxNiveles / 2];
			Rectangle panel;

			void init()
			{
				panelActivo = false;
				panel = { 40 * 4, tiles::lado * 2, tiles::lado * 16, tiles::lado * 12 };
				btnMenuPrincipal = { tiles::lado * 4.5f,tiles::lado * 2.5f,tiles::lado * 3.0f ,tiles::lado * 1.5f };
				for (int i = 0; i < maxNiveles/2; i++)
				{
					if (i < 5)
					{
						btnNiveles[i].rec.x = panel.x + tiles::lado + i * tiles::lado * 3;
						btnNiveles[i].rec.y = panel.y+panel.height/2 - tiles::lado;
					}

					btnNiveles[i].rec.width = tiles::lado * 2;
					btnNiveles[i].rec.height = tiles::lado * 2;
					btnNiveles[i].color = WHITE;
				}
			}

			void update()
			{
				input();
			}

			void input()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					for (int i = 0; i < maxNiveles/2; i++)
					{
						if (CheckCollisionPointRec(mouse, btnNiveles[i].rec))
						{
							nivelm::nivelActual = i+11;
							juego::historia::init();
							menuActual = MENU::JUEGO;
							selectorCarga::panelActivo = false;
						}
					}
					if (CheckCollisionPointRec(mouse, btnMenuPrincipal.rec))
					{
						menues::principal::init();
						menues::principal::panelActivo = false;
						panelActivo = false;
						menues::menuActual = MENU::PRINCIPAL;
					}
				}
			}

			void draw()
			{
				DrawTexturePro(tx::panel, { 0, 0, static_cast<float>(tx::panel.width), static_cast<float>(tx::panel.height) }, panel, { 0, 0 }, 0, WHITE);

				for (int i = 0; i < maxNiveles/2; i++)
				{
					DrawTexturePro(tx::btnNiveles, { static_cast<float>((tx::btnNiveles.width / 10) * i), 0, static_cast<float>(tx::btnNiveles.width / 10), static_cast<float>(tx::btnNiveles.height) }, btnNiveles[i].rec, { 0, 0 }, 0, WHITE);

					Rectangle sourceRec;
					Rectangle destRec = { btnNiveles[i].rec.x, btnNiveles[i].rec.y, tiles::lado , tiles::lado };

				}

				DrawTexturePro(tx::btnMenuPrincipal, { 0, 0, static_cast<float>(tx::btnMenuPrincipal.width), static_cast<float>(tx::btnMenuPrincipal.height) }, btnMenuPrincipal.rec, { 0, 0 }, 0, btnMenuPrincipal.color);
				DrawText("Go Menu", btnMenuPrincipal.rec.x + btnMenuPrincipal.rec.width / 2 - MeasureText("Go Menu", tamFuente) / 2, btnMenuPrincipal.rec.y + btnMenuPrincipal.rec.height / 2 - tamFuente / 2, tamFuente, btnMenuPrincipal.color);

				DrawText(" Level Selector", panel.x + panel.width / 2 - MeasureText("Level Selector", tamFuente * 1.5f) / 2, tiles::tile[0][2].pos.y + tamFuente * 1.5 / 2, tamFuente * 1.5, WHITE);

			}
		}
	}
}





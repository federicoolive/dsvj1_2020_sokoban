#ifndef AUDIO_H
#define AUDIO_H

#include "raylib.h"

namespace sokoban
{
	namespace audio
	{
		enum class TIPOMUSICA { MENU, JUEGO, EDITOR, LIMITE };
		struct MUSICA
		{
			bool activo = true;
			Music musica;
		};
		extern MUSICA musica[static_cast<int>(TIPOMUSICA::LIMITE)];

		enum class TIPOEFECTO { ROTAR, MOVER, EMPUJAR, LIMITE };
		struct EFECTOS
		{
			bool activo = false;
			Sound efecto;
		};
		extern EFECTOS efecto[static_cast<int>(TIPOEFECTO::LIMITE)];

		extern bool musicas;
		extern bool efectos;

		void init();
		void update();
		void deinit();
	}
}

#endif
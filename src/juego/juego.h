#ifndef JUEGO_H
#define JUEGO_H
#include "raylib.h"
#include "tiles/tiles.h"

namespace sokoban
{
	namespace juego
	{
		namespace historia
		{
			void init();
			void play();
			void update();
			void input();
			void draw();
			void deinit();
			bool celdaValida(int posObjetivox, int posObjetivoy, bool dobleChequeo);
			bool chequeoVictoria();
			void cronometro(int tiempo, int tiempoMax);
		}

		namespace editor
		{
			extern tiles::MAPA edit;
			void init();
			void play();
			void update();
			void input();
			void draw();
			void deinit();
			void generadorDeHistoria();
		}
	}
}

#endif
#ifndef LOOP_H
#define LOOP_H

#include "raylib.h"

namespace sokoban
{
	namespace loop
	{
		extern int clockUpdate;
		extern bool exit;
		void juego();
		void update();
	}
}

#endif
#include "texto.h"
#include "configuraciones/configuraciones.h"


namespace match
{
	namespace txt
	{
		LENGUAJE lenguaje = LENGUAJE::ESPA�OL;
		int tamFuente = 30;

		string titulo = "Match 3";
		string btnJugar;
		string btnOpciones;
		string btnCreditos;
		string btnSalir;
		string empezarPartida;
		string muteMusica;
		string muteEfectos;
		string chresoluciones;

		void actualizarLenguaje()
		{
			tamFuente = 15 * config::escalado;
			switch (lenguaje)
			{
			case LENGUAJE::INGLES:
				btnJugar = "Play";
				btnOpciones = "Options";
				btnCreditos = "Credits";
				btnSalir = "Exit";
				empezarPartida = "Press enter to Start.";

				muteMusica = "Press M to mute music.";
				muteEfectos = "Press N to mute effects.";
				chresoluciones = "Resolutions:";
				break;

			case LENGUAJE::ESPA�OL:
				btnJugar = "Jugar";
				btnOpciones = "Opciones";
				btnCreditos = "Creditos";
				btnSalir = "Salir";
				empezarPartida = "Presione enter para comenzar.";

				muteMusica = "Presione M para mutear la musica.";
				muteEfectos = "Presione N para mutear los efectos.";
				chresoluciones = "Resoluciones:";

				break;
			default:
				break;
			}
		}




	}
}
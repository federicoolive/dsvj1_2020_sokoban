#ifndef TILES_H
#define TILES_H

#include "raylib.h"
#include "configuraciones/constantes.h"
#include "configuraciones/general.h"

namespace sokoban
{
	namespace tiles
	{
		struct MAPA
		{
			TILES::TILES_ tipo;
			float rotacion;

			Vector2 pos;
			Color color;
		};
		extern MAPA tile[maxCol][maxFil];

		extern float lado;

		void init();
		void setColor(Color color);
		void draw();
	}
}

#endif
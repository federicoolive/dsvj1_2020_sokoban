#ifndef CONFIGURACIONES_H
#define CONFIGURACIONES_H

#include "raylib.h"

namespace sokoban
{
	namespace config
	{
		extern Vector2 resoluciones[5];
		extern int resolActual;
		extern int fpsTarget;
		extern float escalado;

		void init();
		void update();
		void deInit();
		void preCargaData();
	}
}

#endif
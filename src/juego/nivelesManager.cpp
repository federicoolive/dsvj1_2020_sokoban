#include "nivelesManager.h"
#include <iostream>
#include "configuraciones/configuraciones.h"
#include "menu/menues.h"

namespace sokoban
{
	namespace nivelm
	{
		NIVELREDUCIDO lvlStats[maxNiveles];
		NIVEL nivel;

		int nivelesDesbloqueados = 1;
		int nivelActual = 0;

		void init()
		{
			cargarData();
			setPanelSuperior();
		}

		void deinit()
		{

		}

		void setPasto()
		{
			for (int y = 0; y < maxFil; y++)
			{
				for (int x = 0; x < maxCol; x++)
				{
					tiles::tile[x][y].tipo = TILES::PASTO;
				}
			}
		}

		void setPanelSuperior()
		{
			// VERDES LATERALES
			for (int y = 0; y < alturaPS; y++)
			{
				for (int x = 0; x < espaciadoPS; x++)
				{
					tiles::tile[x][y].tipo = TILES::PASTO;
				}
				for (int x = anchoPS + espaciadoPS; x < maxCol; x++)
				{
					tiles::tile[x][y].tipo = TILES::PASTO;
				}
			}


			for (int y = 0; y < alturaPS; y++)
			{
				for (int x = espaciadoPS; x < anchoPS + espaciadoPS; x++)
				{
					if (y == alturaPS - 1)	// BORDE INF
					{
						tiles::tile[x][y].tipo = TILES::LAT;
						tiles::tile[x][y].rotacion = 270;
					}
					else if (x == espaciadoPS)	// BORDE IZQ
					{
						tiles::tile[x][y].tipo = TILES::LAT;
						tiles::tile[x][y].rotacion = 0;
					}
					else if (x == anchoPS + espaciadoPS - 1)	// BORDE DER
					{
						tiles::tile[x][y].tipo = TILES::LAT;
						tiles::tile[x][y].rotacion = 180;
					}
					else	// TODO LO DE ADENTRO
					{
						tiles::tile[x][y].tipo = static_cast<TILES::TILES_>(GetRandomValue(TILES::SUELO_MIN, TILES::SUELO_MAX));
					}
				}
			}
						// ESQUINAS
			tiles::tile[espaciadoPS][alturaPS - 1].tipo = TILES::ESQP;
			tiles::tile[anchoPS + espaciadoPS - 1][alturaPS - 1].tipo = TILES::ESQP;
			tiles::tile[anchoPS + espaciadoPS - 1][alturaPS - 1].rotacion = 180;
		}

		void setPanelEditor()
		{
			tiles::tile[0][1].tipo = TILES::PASTO;
			tiles::tile[1][1].tipo = TILES::SUELO_MIN;
			tiles::tile[2][1].tipo = TILES::OBJETIVO;
			tiles::tile[3][1].tipo = TILES::ESQP;
			tiles::tile[4][1].tipo = TILES::ESQC;
			tiles::tile[5][1].tipo = TILES::LAT;
			tiles::tile[6][1].tipo = TILES::AGUJERO;
		}

		void guardarData()
		{
			int dataIndex = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			std::cout << "Inicio: " << dataIndex << std::endl;
			SaveStorageValue(dataIndex, 0);		// Tiempo - Sacar, se guarda al final del juego.
			dataIndex++;
			SaveStorageValue(dataIndex, 0);		// Movimientos - sacar, se guarda al final del juego.
			dataIndex++;
			SaveStorageValue(dataIndex, 0);		// Tiempo Max
			dataIndex++;
			SaveStorageValue(dataIndex, 0);		// Movimientos Max
			dataIndex++;
			SaveStorageValue(dataIndex, 0);		// Completado? - sacar, se guarda al final del juego.
			dataIndex++;

			for (int y = 0; y < maxFil; y++)
			{
				for (int x = 0; x < maxCol; x++)
				{
					SaveStorageValue(dataIndex, tiles::tile[x][y].tipo);
					dataIndex++;
					SaveStorageValue(dataIndex, tiles::tile[x][y].rotacion);
					dataIndex++;
				}
			}
			SaveStorageValue(dataIndex, obj::jugador.pos.x);
			dataIndex++;
			SaveStorageValue(dataIndex, obj::jugador.pos.y);
			dataIndex++;
			
			for (int i = 0; i < maxObjetos; i++)
			{
				SaveStorageValue(dataIndex, obj::autos[i].pos.x);
				dataIndex++;
				SaveStorageValue(dataIndex, obj::autos[i].pos.y);
				dataIndex++;
			}
			SaveStorageValue(dataIndex, nivel.maxObj);
			dataIndex++;
			std::cout << "Fin: " << dataIndex << std::endl;
		}

		void cargarData()
		{
			int dataIndex = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			std::cout << "Inicio de Carga: " << dataIndex << std::endl;
			//SaveStorageValue(dataIndex, 0);// Tiempo - Sacar, se guarda al final del juego.
			dataIndex++;
			//SaveStorageValue(dataIndex, 0);// Movimientos - sacar, se guarda al final del juego.
			dataIndex++;
			nivel.tiempoMax = LoadStorageValue(dataIndex);
			dataIndex++;
			nivel.movMax = LoadStorageValue(dataIndex);
			dataIndex++;
			//SaveStorageValue(dataIndex, 0);// Completado? - sacar, se guarda al final del juego.
			dataIndex++;

			for (int y = 0; y < maxFil; y++)
			{
				for (int x = 0; x < maxCol; x++)
				{
					tiles::tile[x][y].tipo = static_cast<TILES::TILES_>(LoadStorageValue(dataIndex));
					dataIndex++;
					tiles::tile[x][y].rotacion = LoadStorageValue(dataIndex);
					dataIndex++;
				}
			}

			obj::jugador.pos.x = LoadStorageValue(dataIndex);
			dataIndex++;
			obj::jugador.pos.y = LoadStorageValue(dataIndex);
			dataIndex++;

			for (int i = 0; i < maxObjetos; i++)
			{
				obj::autos[i].pos.x= LoadStorageValue(dataIndex);
				dataIndex++;
				obj::autos[i].pos.y = LoadStorageValue(dataIndex);
				dataIndex++;
			}
			nivel.maxObj = LoadStorageValue(dataIndex);

			std::cout << "Fin de Carga: " << dataIndex << std::endl;
		}

		void guardarNivelActual()
		{
			int indexData = 0;
			if (nivelActual == nivelesDesbloqueados - 1)
			{
				nivelesDesbloqueados++;
			}
			SaveStorageValue(indexData, nivelesDesbloqueados);

			lvlStats[nivelActual].completado = true;
			lvlStats[nivelActual].movimientos = (nivel.movimientos <= nivel.movMax) ? true : false;
			lvlStats[nivelActual].tiempo = (nivel.tiempos[1] - nivel.tiempos[0] <= nivel.tiempoMax) ? true : false;
			
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData, nivel.tiempos[1] - nivel.tiempos[0]);
			indexData++;
			SaveStorageValue(indexData, nivel.movimientos);
			indexData += 3;
			SaveStorageValue(indexData, true);

			
		}

		void cargarJugador()
		{
			int indexData = 0;

			nivelesDesbloqueados = LoadStorageValue(indexData);
			config::preCargaData();

		}

		void reStartNivelesCreados()	// Creada para reiniciar el Storage value antes de la subida.
		{ 
			// Management Game Design:
			// https://docs.google.com/spreadsheets/d/1oZ-xa9eHIWKx9YU2EBLA9NH_1OodbdbkghVcv0XdkoE/edit#gid=0
			
			int indexData = 0;
			SaveStorageValue(0, 1);

			// Los indices:
			int tiempoUser = 0;
			int movUser = 1;
			int movMax = 3;
			int timMax = 2;
			int compet = 4;

			nivelActual = 0;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 30);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 15 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 1;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 53);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 20 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 2;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 26);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 9 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 3;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 30);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 11 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 4;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 70);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 30 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 5;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 132);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 58 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 6;
			indexData = storageInicioNivel + nivelActual * storageTamaņoNiveles;
			SaveStorageValue(indexData + tiempoUser, 0);
			SaveStorageValue(indexData + movUser, 0);
			SaveStorageValue(indexData + movMax, 165);	// Relativo a Nivel
			SaveStorageValue(indexData + timMax, 168 * 1000);	// Relativo a Nivel
			SaveStorageValue(indexData + compet, 0);

			nivelActual = 0;
			cargarJugador();
		}
	}
}
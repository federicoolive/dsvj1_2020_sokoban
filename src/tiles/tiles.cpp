#include "tiles.h"
#include "texturas/texturas.h"

namespace sokoban
{
	namespace tiles
	{
		bool activo;
		bool visibilidad;
		float lado = 40;
		MAPA tile[maxCol][maxFil];
		
		void init()
		{
			for (int y = 0; y < maxFil; y++)
			{
				for (int x = 0; x < maxCol; x++)
				{
					tile[x][y].pos.x = lado * x + lado / 2;
					tile[x][y].pos.y = lado * y + lado / 2;
					tile[x][y].color = WHITE;
				}
			}

			for (int x = 0; x < maxCol; x++)
			{
				tile[x][0].rotacion = 90;
				tile[x][0].tipo = TILES::LAT;

				tile[x][maxFil - 1].rotacion = 270;
				tile[x][maxFil - 1].tipo = TILES::LAT;
			}
			for (int y = 0; y < maxFil; y++)
			{
				tile[0][y].rotacion = 0;
				tile[0][y].tipo = TILES::LAT;

				tile[maxCol - 1][y].tipo = TILES::LAT;
				tile[maxCol - 1][y].rotacion = 180;
			}
		}

		void setColor(Color color)
		{
			for (int y = 0; y < maxFil; y++)
			{
				for (int x = 0; x < maxCol; x++)
				{
					tile[x][y].color = color;
				}
			}
		}

		void draw()
		{
			for (int y = 0; y < maxFil; y++)
			{
				for (int x = 0; x < maxCol; x++)
				{
					DrawTexturePro(tx::tile[tile[x][y].tipo], { 0, 0, lado, lado }, { tile[x][y].pos.x, tile[x][y].pos.y, lado, lado }, { lado / 2, lado / 2 }, tile[x][y].rotacion, tile[x][y].color);
				}
			}
		}
	}
}
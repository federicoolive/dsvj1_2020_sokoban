#ifndef TEXTURAS_H
#define TEXTURAS_H

#include "raylib.h"
#include "configuraciones/constantes.h"

namespace sokoban
{
	namespace tx
	{
		extern Texture2D btnMenuPrincipal;
		extern Texture2D btnNiveles;
		extern Texture2D estrella;
		extern Texture2D panel;
		extern Texture2D candado;

		extern Texture2D fondo[FONDOS::LIMITE];
		extern Texture2D tile[TILES::LIMITE];
		extern Texture2D grua;
		extern Texture2D autos;

		void init();
		void update();
		void deinit();
	}
}

#endif
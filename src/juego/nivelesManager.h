#ifndef NIVELES_MANAGER_H
#define NIVELES_MANAGER_H

#include "raylib.h"
#include "tiles/tiles.h"
#include "configuraciones/constantes.h"
#include "juego/objetos.h"

namespace sokoban
{
	namespace nivelm
	{
		extern int nivelesDesbloqueados;
		extern int nivelActual;

		struct TILE
		{
			TILES::TILES_ tipo;
			float rot;
		};
		struct OBJ
		{
			Vector2 pos;
			OBJTIPO::OBJTIPO_ tipo;
		};

		struct NIVELREDUCIDO
		{
			bool completado = false;
			bool tiempo = false;
			bool movimientos = false;
		};
		extern NIVELREDUCIDO lvlStats[maxNiveles];

		struct NIVEL
		{
			bool completado;
			int tiempos[2];
			int movimientos = 0;

			int tiempoMax;
			int movMax;
			TILE tiles[maxCol][maxFil];
			
			Vector2 posPlayer;

			int maxObj;
			OBJ objeto[maxObjetos];
		};
		extern NIVEL nivel;

		void init();
		void deinit();
		void setPasto();
		void setPanelSuperior();
		void setPanelEditor();
		void guardarData();
		void cargarData();
		void guardarNivelActual();
		void cargarJugador();
		void reStartNivelesCreados();
	}
}

#endif
#ifndef OBJETOS_H
#define OBJETOS_H

#include "raylib.h"
#include "configuraciones/estructuras.h"
#include "configuraciones/constantes.h"

namespace sokoban
{
	namespace obj
	{
		struct OBJETOS
		{
			bool activo = false;
			bool visibilidad = false;
			bool enMovimiento;
			bool enRotacion;

			OBJTIPO::OBJTIPO_ tipo;
			Rectangle sourceRec;
			Rectangle destRec;

			Vector2 pos;
			float rotacion;

			float rotObjetivo;
			Vector2 posObjetivo; 
			Color color;
			int direccion;
		};

		extern OBJETOS jugador;
		extern OBJETOS autos[maxObjetos];

		void init();
		void establecerUnObjeto(int i);
		void update();
		void draw();
		bool rotarJugador();
		bool moverJugador();
	}
}

#endif
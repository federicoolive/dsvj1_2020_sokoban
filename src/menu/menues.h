#ifndef MENUES_H
#define MENUES_H

#include "raylib.h"
#include "configuraciones/constantes.h"

namespace sokoban
{
	namespace menues
	{
		extern int menuActual;

		void init();

		namespace principal
		{
			extern bool panelActivo;
			void init();
			void update();
			void input();
			void draw();
			void deinit();
		}

		namespace selectorNiveles
		{
			void init();
			void update();
			void input();
			void draw();
		}

		namespace selectorCarga
		{
			extern bool panelActivo;
			void init();
			void update();
			void input();
			void draw();
		}
	}
}




#endif
#include "juego/objetos.h"
#include "texturas/texturas.h"
#include "tiles/tiles.h"
#include "juego/nivelesManager.h"

namespace sokoban
{
	namespace obj
	{
		OBJETOS jugador;
		OBJETOS autos[maxObjetos];

		void init()
		{
			jugador.tipo = OBJTIPO::JUGADOR;
			jugador.sourceRec = { 0, 0, static_cast<float>(tx::grua.width), static_cast<float>(tx::grua.height) }; 
			jugador.destRec = { 0, 0, static_cast<float>(tx::grua.width), static_cast<float>(tx::grua.height) };
			jugador.rotacion = 90 * GetRandomValue(0, 3);
			jugador.direccion = jugador.rotacion / 90;
			jugador.color = WHITE;
			jugador.posObjetivo = jugador.pos;
			jugador.rotObjetivo = jugador.rotacion;

			for (int i = 0; i < nivelm::nivel.maxObj; i++)
			{
				establecerUnObjeto(i);
			}
		}

		void establecerUnObjeto(int i)
		{
			autos[i].tipo = OBJTIPO::AUTO;

			Vector2 auxSourceRec = { autosPix * GetRandomValue(0, maxAutosH), autosPix * GetRandomValue(0, maxAutosV) };
			autos[i].sourceRec = { auxSourceRec.x, auxSourceRec.y, static_cast<float>(tx::autos.width / maxAutosH), static_cast<float>(tx::autos.height / maxAutosV) };

			autos[i].destRec = { 0, 0, tiles::lado + 10, tiles::lado };
			autos[i].rotacion = 90 * GetRandomValue(0, 3);
			autos[i].color = WHITE;
			autos[i].posObjetivo = autos[i].pos;
			autos[i].rotObjetivo = autos[i].rotacion;
		}

		void update()
		{
			jugador.destRec.x = jugador.pos.x;
			jugador.destRec.y = jugador.pos.y;

			for (int i = 0; i < nivelm::nivel.maxObj; i++)
			{
				autos[i].destRec.x = autos[i].pos.x;
				autos[i].destRec.y = autos[i].pos.y;
			}
		}

		void draw()
		{
			DrawTexturePro(tx::grua, jugador.sourceRec, jugador.destRec, { jugador.sourceRec.width / 2, jugador.sourceRec.height / 2 }, jugador.rotacion, obj::jugador.color);
			for (int i = 0; i < nivelm::nivel.maxObj; i++)
			{
				DrawTexturePro(tx::autos, autos[i].sourceRec, autos[i].destRec, { autos[i].destRec.width / 2, autos[i].destRec.height / 2 }, autos[i].rotacion, obj::autos[i].color);
			}
		}

		bool rotarJugador()
		{
			if (jugador.rotacion >= 270 && jugador.direccion == DIRECCION::ARRIBA)
			{
				jugador.rotacion += velAnimRotJugador;
			}
			else if (jugador.rotacion <= 0 && jugador.direccion == DIRECCION::IZQUIERDA)
			{
				jugador.rotacion -= velAnimRotJugador;
			}
			else if (jugador.rotObjetivo - jugador.rotacion > jugador.rotacion - jugador.rotObjetivo)
			{
				jugador.rotacion += velAnimRotJugador;
			}
			else
			{
				jugador.rotacion -= velAnimRotJugador;
			}

			if (jugador.rotacion >= 360)
			{
				jugador.rotacion -= 360;
			}
			else if (jugador.rotacion < 0)
			{
				jugador.rotacion += 360;
			}

			if (jugador.rotacion == jugador.rotObjetivo)
			{
				return false;
			}
			return true;
		}

		bool moverJugador()
		{
			switch (jugador.direccion)
			{
			case DIRECCION::ARRIBA:

				jugador.pos.y -= velAnimMovJugador;

				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					if (obj::autos[i].enMovimiento)
					{
						obj::autos[i].pos.y -= velAnimMovJugador;
					}
				}

				break;
			case DIRECCION::DERECHA:

				jugador.pos.x += velAnimMovJugador;

				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					if (obj::autos[i].enMovimiento)
					{
						obj::autos[i].pos.x += velAnimMovJugador;
					}
				}

				break;
			case DIRECCION::ABAJO:

				jugador.pos.y += velAnimMovJugador;

				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					if (obj::autos[i].enMovimiento)
					{
						obj::autos[i].pos.y += velAnimMovJugador;
					}
				}

				break;
			case DIRECCION::IZQUIERDA:

				jugador.pos.x -= velAnimMovJugador;

				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					if (obj::autos[i].enMovimiento)
					{
						obj::autos[i].pos.x -= velAnimMovJugador;
					}
				}

				break;

			default:
				break;
			}

			if (jugador.pos.x == jugador.posObjetivo.x && jugador.pos.y == jugador.posObjetivo.y)
			{
				for (int i = 0; i < nivelm::nivel.maxObj; i++)
				{
					obj::autos[i].enMovimiento = false;
				}
				return false;
			}
			return true;
		}
	}
}